﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEditor;
using System.Linq;

namespace Tests
{
    public class TestPlaymode
    {
        static string[] GetEnabledScenes() {
            return (
                from scene in EditorBuildSettings.scenes
                where scene.enabled
                where !string.IsNullOrEmpty(scene.path)
                select scene.path
            ).ToArray();
        }

        [UnityTest]
        public IEnumerator UnitTest() {
            bool success = true;
            foreach(string scenePath in GetEnabledScenes()) {
                string[] path = scenePath.Split('/');
                string sceneToTest = path[path.Length - 1].Split('.')[0];
                SceneManager.LoadScene(sceneToTest);
                yield return null;
                success &= (sceneToTest == SceneManager.GetActiveScene().name);
            }
            Assert.IsTrue(success);
        }

        [UnityTest]
        public IEnumerator IntegrationTest() {
            WWWForm form = new WWWForm();
            form.AddField("username", "test");
            form.AddField("password", "test");

            string url = "https://unitytestforms.000webhostapp.com/login.php";
            bool success = false;

            using(UnityWebRequest www = UnityWebRequest.Post(url, form)) {
                yield return www.SendWebRequest();

                string response = www.downloadHandler.text;
                if(!www.isNetworkError) {
                    if(response == "0") {//Success
                        success = true;
                    }
                }
            }

            Assert.IsTrue(success);
        }

    }
}
