﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DatabaseHandler : MonoBehaviour 
{
    public Text infoLabel;
    public Text debugLabel;

    public InputField usernameField;
    public InputField passwordField;
    
    public Button playButton;

    public void CallRegister() {
        StartCoroutine(Register(usernameField.text, passwordField.text));
    }

    IEnumerator Register(string username, string password) {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        string url = "https://unitytestforms.000webhostapp.com/register.php";

        using(UnityWebRequest www = UnityWebRequest.Post(url, form)) {

            yield return www.SendWebRequest();

            string response = www.downloadHandler.text;
            if(www.isNetworkError) {
                Debug.LogWarning("Network Error");
                debugLabel.text = "Network Error";
            }
            else {
                if(response == "0") {//Success
                    Debug.Log("User " + usernameField.text + " registered");
                    infoLabel.text = "User registered successfully";
                }
                else {
                    debugLabel.text = response;
                    Debug.LogWarning("[" + response + "] User " + usernameField.text + " not registered");
                    infoLabel.text = "Registration failed";
                }
            }
        }
    }

    public void CallLogin() {
        StartCoroutine(Login(usernameField.text, passwordField.text));
    }

    public void CallLogin(string username, string password) {
        StartCoroutine(Login(username, password));
    }

    IEnumerator Login(string username, string password) {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        string url = "https://unitytestforms.000webhostapp.com/login.php";

        //WWW www = new WWW(url, form);
        using(UnityWebRequest www = UnityWebRequest.Post(url, form)) {
            yield return www.SendWebRequest();

            string response = www.downloadHandler.text;
            if(www.isNetworkError) {
                Debug.LogWarning("Network Error");
                debugLabel.text = "Network Error";
            }
            else {
                if(response == "0") {//Success
                    Debug.Log("User " + usernameField.text + " logged in");
                    infoLabel.text = "User logged in successfully";
                    playButton.interactable = true;
                }
                else {
                    debugLabel.text = response;
                    Debug.LogWarning("[" + response + "] User " + usernameField.text + " not logged in");
                    infoLabel.text = "Login failed";
                }
            }
        }
    }
}
