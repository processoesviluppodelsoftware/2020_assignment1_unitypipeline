# Unity Pipeline 

Componenti del gruppo:
* Lisa Cocchia - 793569
* Emanuele Giannuzzi - 797819

## Descrizione applicazione 

[FPS Microgame](https://learn.unity.com/project/fps-template) è un gioco sparatutto 3D in prima persona.
È uno dei template open source messi a disposizione da *Unity Learn*, come tutorial per il game engine.
È stato modificato da noi, aggiungendo una UI che richiede di registrarsi e fare il login prima di poter giocare. I dati di login sono immagazzinati in un database MySQL e, per motivi di sicurezza, la connessione ad esso avviene attraverso uno script *php* hostato su un web server.

## Realizzazione pipeline

La pipeline realizzata si basa sul template [`unity3d-gitlab-ci-example`](https://gitlab.com/gableroux/unity3d-gitlab-ci-example), realizzato da [Gabriel Le Breton](https://gitlab.com/gableroux).
Il template fornisce metodi di base per la fase di build e test del progetto, attraverso script e immagini docker create ad hoc per l'integrazione tra *GitLab* ed il *game engine Unity*. 
È stato comunque necessario modificare sostanzialmente il template, in quanto non si adattava alle nostre esigenze. 
<br/><br/>L'analisi del codice viene effettuata attraverso la piattaforma [*SonarQube*](https://www.sonarqube.org/), lanciando la versione CLI (*Command Line Interface*) dello strumento [*SonarScanner*](https://github.com/SonarSource/sonar-scanner-cli) e mettendolo in comunicazione con un server [*SonarCloud*](https://sonarcloud.io/). Esso fornisce una web interface da cui è possibile visualizzare il report dell'analisi.
<br/><br/> Il progetto viene compilato per le piattaforme *Windows64* e *WebGL*. Il primo genera un eseguibile che viene rilasciato attraverso un servizio di file hosting, mentre il secondo viene reso pubblico attraverso un sito web statico. 
<br/><br/> Come servizio di file hosting è stato fatto uso di [Amazon Web Services](https://aws.amazon.com/), in particolare del servizio *S3*. È possibile scaricare costantemente il pacchetto dell'ultima versione al seguente link: [https://unitypipeline.s3.eu-west-3.amazonaws.com/2020_assignment1_unitypipeline-StandaloneWindows64.zip](https://unitypipeline.s3.eu-west-3.amazonaws.com/2020_assignment1_unitypipeline-StandaloneWindows64.zip).
<br/><br/>Il sito WebGL, nella fase di deploy, viene distribuito attraverso il sito web [http://unitylogintest.s3-website.eu-west-3.amazonaws.com/](http://unitylogintest.s3-website.eu-west-3.amazonaws.com/), da cui è possibile accedere direttamente all'ultima versione del gioco.


### Stage pipeline

* **build**: Compila l'applicazione per le piattaforme Windows64 e WebGL(genera un sito web già pronto a far partire l'applicazione), generando anche tutti i file necessari a lanciare l'applicazione.

* **verify**: Lancia `sonar-scanner-cli` che esegue l'analisi sulla qualità del codice. Viene generato un link di SonarCloud nel log della console, che permette di esaminare i risultati dell'analisi.

* **unit-test**: Prende l'elenco delle scene abilitate, le carica una alla volta, controllando che siano attive.

* **integration-test**: Invia una richiesta di login al database, con un nome utente e password sicuramente presenti nel database e controlla che vada a buon fine.

* **package**: Crea un file zip utilizzando l'artefatto generato dal job *build-StandaloneWindows64*, in fase di build. 

* **release**: Carica il file zip generato da package su un servizio di file hosting.

* **deploy**: Carica su un sito web statico (bucket s3 configurato come tale) l'artefatto generato dal job *build-WebGL*, da cui è possibile utilizzare l'applicazione.


## Problemi riscontrati

Essendo GitLab uno strumento poco utilizzato per la creazione di pipeline CI/CD per Unity, la documentazione è sia molto scarsa che sparsa e questo ha portato a numerosi rallentamenti durante lo sviluppo della pipeline.
<br/><br/>Il primo scoglio è stato trovare un software Lint per C# in quanto i tool disponibili sono spesso dei pacchetti *NuGet* per *Visual Studio*.
La scelta è ricaduta su *SonarQube*, in quanto *open-source* e fornisce un'immagine docker ufficiale per *SonarScanner*, un'applicazione client che si interfaccia con il server *SonarQube* per eseguire l'analisi. Abbiamo riscontrato problemi anche a lanciare un server *SonarQube* in ambiente CI prima di scoprire *SonarCloud*, un servizio online che fornisce server *SonarQube*, anche gratuitamente per progetti pubblici.
<br/><br/>Nonostante il template sia ben sviluppato, la fase di build ha dato non pochi problemi. 
<br/>L'immagine docker di Unity fornita dal template è incompatibile con *Specific Runner* che girano in ambiente Windows ed i tempi di esecuzione del job possono diventare molto lunghi utilizzando Runner GitLab a causa della dimensione dell'immagine docker per *Unity* (circa 2GB).
<br/><br/>L'implementazione per WebGL per le API di *Unity*, per motivi di sicurezza, richiedono che il server ricevente abbia configurato *CORS (Cross-Origin Resource Sharing)* correttamente, in modo da poter accettare richieste da domini differenti dal proprio. Questo ha portato via molto tempo in quanto è un problema poco comune ed è stato difficile identificarlo.
<br/><br/>Abbiamo riscontrato limitazioni nell'uso degli Specific Runner. In particolare, avevamo bisogno di limitare l'accesso ad alcuni job ad uno dei nostri Runner, ma al momento l'unico modo per correlare job e Runner è quello di assegnare dei tag ad entrambi, ma questa feature è troppo limitata e non ci permette di raggiungere il nostro obbiettivo. Alla fine, abbiamo optato per eliminare tutti i tag e non utilizzare il Runner che creava problemi.






